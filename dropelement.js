function dropElements(arr, func) {   
    let i = 0;
        while (true) {
            let result = func(arr[0]);
            if (result) {
                break;
            } else {
                arr.splice(0,1);
            }
            if(i > arr.length) {arr=[]; break;}
            i++;
        }
        return arr;
  }


let result = dropElements([1, 2, 3, 4], (n) => {return n >= 3;})
console.log(result);
result = dropElements([0, 1, 0, 1], (n) => {return n === 1;})
console.log(result);
result = dropElements([1, 2, 3, 7, 4], (n) => {return n > 3;})
console.log(result);
result = dropElements([1, 2, 3, 9, 2], function(n) {return n > 2;})
console.log(result);
result = dropElements([1, 2, 3, 4], function(n) {return n > 5;})
console.log(result);