function translatePigLatin(str) {
    const vowels = ['a','e','i','o','u'];
    if (vowels.indexOf(str[0])!==-1) return str+'way'; 
    else {
        let begin = str[0];
        let helper = 1;
        let vowel = false;

        while (!vowel){
            if (helper=== (str.length-1)) {
                return str + 'ay';
                vowel = true;
                
            }

            if (vowels.indexOf(str[helper])!==-1)vowel = true;
            else begin = begin + str[helper];
            helper++;
        }
        return str.slice(helper-1) + begin + 'ay';
    }
  }


  let result = translatePigLatin('algorithm');
  console.log(result);
  result = translatePigLatin("california");
  console.log(result);
  result = translatePigLatin("rhythm");
  console.log(result);
  result = translatePigLatin("schwartz");
  console.log(result);
  