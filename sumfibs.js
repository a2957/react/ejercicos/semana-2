const fibonacci = (n) => {
    return Math.floor(((1+Math.sqrt(5))**n-(1-Math.sqrt(5))**n)/(2**n*Math.sqrt(5)))
}

const secFibo = (n) => {
    let sum = 0;

    let num = n;
    let number;
    while (n > 0) {
        if (n!==0){
            number = fibonacci(n)
            if ( number % 2 === 1 && number < num){
                sum = sum + number;
            }
        }
        n--;
    }
    return sum
}

const result = secFibo(1000)
console.log(result)